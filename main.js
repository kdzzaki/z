function start(){
  var elm=document.getElementById("upload_img_form");
  var image=elm.children[0].files[0];
  var url=elm.children[1].value;
  if(image && url){
    hide_upload_img_form();
    var img=document.createElement("img");
    img.src = URL.createObjectURL(image);
    img.onload = function() {
      URL.revokeObjectURL(this.src);
      document.getElementById("preview").style="width:"+this.offsetWidth+"px;height:"+this.offsetHeight+"px;"
      document.getElementById("design_image_width").style="width:"+this.offsetWidth+"px;height:"+this.offsetHeight+"px;"
      document.getElementById("design_image_width").max=""+this.offsetWidth;
      document.getElementById("design_image_width").value=""+(this.offsetWidth/2);
      set_desing_image_width();
      document.getElementById("design_image_width").oninput=function(){set_desing_image_width();};
    }
    img.setAttribute("alt",image.name);
    document.getElementById("design_image").innerHTML="";
    document.getElementById("design_image").append(img);
    document.getElementById("preview").setAttribute("src",url);
  }else{
    alert("please select an image..\nand enter the html page url");
  }
}
function show_upload_img_form(){
  document.getElementById("select_img").setAttribute("class","hidden");
  document.getElementById("upload_img_form").removeAttribute("class");
}
function hide_upload_img_form(){
  document.getElementById("upload_img_form").setAttribute("class","hidden");
  document.getElementById("select_img").removeAttribute("class");
}
function set_desing_image_width(){
  document.getElementById("design_image").style.width=""+document.getElementById("design_image_width").value+"px";
}